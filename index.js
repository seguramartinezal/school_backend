const express = require("express");
const app = express();
const port = 4000;
const estudiante = require("./actionMethods/estudiante");
const materia = require("./actionMethods/materia");
const curso = require("./actionMethods/curso");
const nota = require("./actionMethods/nota");
const inscripcion = require('./actionMethods/Inscripcion');
const calificacion = require('./actionMethods/calificacion');
const asistencia = require('./actionMethods/asistencia');
const historialAsistencia = require('./actionMethods/historialAsistencia');


app.use(estudiante);
app.use(materia);
app.use(curso);
app.use(nota);
app.use(inscripcion);
app.use(calificacion);
app.use(asistencia);
app.use(historialAsistencia);

app.listen(port, () => {
    console.log("La app esta corriendo en el puerto " + port);
})