const express = require("express");
const app = express();
const cors = require("cors");
const bodyparser = require("body-parser");
const Connection = require('D:/pruebatecnica/ptecnicabackend/connection/conexion.js');



app.use(cors());
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());

const pool = Connection.pool;

app.get('/Inscripcion', (req, res) => {

    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        const response = await client.query(`SELECT e."Nombre" ||' ' || e."Apellido" || ' ' || e."Matricula" Estudiante , g."Nombre" Grado, 
        eec."Id_Estudiante_en_curso"  Id FROM "Estudiante_En_Curso" eec join "Estudiante" e on eec."Id_Estudiante" = e."Id_estudiante" 
        LEFT JOIN "Grado" g on eec."Id_Grado" = g."ID_Grado"`)

        done();

        if(err) {
            return console.error('error running query', err);
        }
        console.log(response.rows);
        res.send(response.rows);
    })
})

app.get('/Inscripciones', (req, res) => {

    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        const response = await client.query(`SELECT "ID_Grado" id, "Nombre" grado FROM "Grado";`)

        done();

        if(err) {
            return console.error('error running query', err);
        }
        console.log(response.rows);
        res.send(response.rows);
    })

})


app.put("/ActualizarGrado", (req, res) => {

    const json = {
        success: false,
        message: ""
    }

    console.log(req.body);

    return pool.connect(async (err, client, done) => {

         if(err) {
             return console.error('connexion error', err);
         }

         await client.query(`UPDATE "Estudiante_En_Curso" SET "Id_Grado"=${req.body.data.idGrado} 
                            WHERE "Id_Estudiante_en_curso"=${req.body.data.idEstudiante}`)

         await client.query(`INSERT INTO "Materia_Estudiante_Grado" ("Id_Estudiante_Curso", "Id_Materia")
                                VALUES(${req.body.data.idEstudiante}, (select m."Id_Materia"  from "Materia" m where "Nombre" = 'Matematica')),
                                (${req.body.data.idEstudiante}, (select m."Id_Materia"  from "Materia" m where "Nombre" = 'Literatura')),
                                (${req.body.data.idEstudiante}, (select m."Id_Materia"  from "Materia" m where "Nombre" = 'Naturales')),
                                (${req.body.data.idEstudiante}, (select m."Id_Materia"  from "Materia" m where "Nombre" = 'Sociales'))`)

         done();

         if(err) {
             json.success = false;
             json.message = "Error al actualizar registro: " + err;
             res.send(json);
             return console.error('error running query', err);
         }

         json.success = true;
         json.message = "Estudiante actualizado";


         res.send(json);
     })

})







module.exports = app;