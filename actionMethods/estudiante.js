const express = require("express");
const app = express();
const cors = require("cors");
const bodyparser = require("body-parser");
const Connection = require('D:/pruebatecnica/ptecnicabackend/connection/conexion.js');



app.use(cors());
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());



const pool = Connection.pool;
const client = Connection.client;


app.get('/ConsultarEstudiantes', (req, res) => {
    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        const response = await client.query(`SELECT * FROM public."Estudiante"`)

        done();

        if(err) {
            return console.error('error running query', err);
        }
       res.send(response.rows);
    })

})

app.post("/RegistrarEstudiantes",   (req, res) => {


    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        const response = await client.query(`SELECT COUNT(*) FROM public."Estudiante" WHERE "Matricula" = '${req.body.matricula}'`);

        if (response.rows[0].count === String(1)){
            res.send("false");
        }else{
            await client.query(`INSERT INTO public."Estudiante"("Nombre", "Apellido", "Matricula")
	                      VALUES ('${req.body.nombre}', '${req.body.apellido}', '${req.body.matricula}')`)

            await client.query(`INSERT INTO public."Estudiante_En_Curso" ("Id_Estudiante", "Id_Grado")
        VALUES((select "Id_estudiante" from "Estudiante" order by "Id_estudiante" desc limit 1), null)`)

            res.send("true");
        }
        
        done();

        if(err) {
            return console.error('error running query', err);
        }


    })


    /*return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        await client.query(`INSERT INTO public."Estudiante"("Nombre", "Apellido", "Matricula")
	                      VALUES ('${req.body.nombre}', '${req.body.apellido}', '${req.body.matricula}')`)

       const response = await client.query(`INSERT INTO public."Estudiante_En_Curso" ("Id_Estudiante", "Id_Grado")
        VALUES((select "Id_estudiante" from "Estudiante" order by "Id_estudiante" desc limit 1), null)`)

        done();

        if(err) {
            return console.error('error running query', err);
        }

        res.send("true");
    })*/

})

app.put("/ConsultarEstudiantes", (req, res) => {

    const json = {
        success: false,
        message: ""
    }

      return pool.connect(async (err, client, done) => {

          if(err) {
              return console.error('connexion error', err);
          }

          await client.query(`UPDATE public."Estudiante" SET "Nombre"='${req.body.data.nombre}',
                  "Apellido"='${req.body.data.apellido}', "Matricula"='${req.body.data.matricula}' WHERE "Id_estudiante"=${req.body.data.id}`)

          done();

          if(err) {
              json.success = false;
              json.message = "Error al actualizar registro: " + err;
              res.send(json);
              return console.error('error running query', err);
          }

          json.success = true;
          json.message = "Estudiante actualizado";


          res.send(json);
      })

})

app.delete("/ConsultarEstudiantes", (req, res) => {

    const id = req.body.data;
    const json = {
        success: false,
        message: ""
    }


    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        await client.query(`DELETE FROM public."Estudiante" WHERE "Id_estudiante" = ${id}`)

        done();

        if(err) {
            json.success = false;
            json.message = "Error al borrar el registro: " + err;
            res.send(json);
            return console.error('error running query', err);
        }

        json.success = true;
        json.message = "Estudiante borrado";

        res.send(json);
    })


})


module.exports = app;
