const express = require("express");
const app = express();
const cors = require("cors");
const bodyparser = require("body-parser");
const Connection = require('D:/pruebatecnica/ptecnicabackend/connection/conexion.js');



app.use(cors());
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());



const pool = Connection.pool;
const client = Connection.client;


app.get('/ConsultarMateria', (req, res) => {
    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        const response = await client.query(`SELECT * FROM public."Materia"`)

        done();

        if(err) {
            return console.error('error running query', err);
        }
        res.send(response.rows);
    })

})

app.post("/RegistrarMateria", (req, res) => {
    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        await client.query(`INSERT INTO public."Materia" ("Nombre") VALUES ('${req.body.nombre}')`)

        done();

        if(err) {
            return console.error('error running query', err);
        }

        res.send("true");
    })

})

app.put("/ConsultarMateria", (req, res) => {

    const json = {
        success: false,
        message: ""
    }
    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        await client.query(`UPDATE public."Materia" SET "Nombre"='${req.body.data.nombre}' WHERE "Id_Materia"=${req.body.data.id}`)

        done();

        if(err) {
            json.success = false;
            json.message = "Error al actualizar registro: " + err;
            res.send(json);
            return console.error('error running query', err);
        }

        json.success = true;
        json.message = "Materia actualizada";


        res.send(json);
    })


})

app.delete("/ConsultarMateria", (req, res) => {

    const id = req.body.data;
    const json = {
        success: false,
        message: ""
    }

    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        await client.query(`DELETE FROM public."Materia" WHERE "Id_Materia" = ${id}`)

        done();

        if(err) {
            json.success = false;
            json.message = "Error al borrar el registro: " + err;
            res.send(json);
            return console.error('error running query', err);
        }

        json.success = true;
        json.message = "Materia borrada";

        res.send(json);
    })



})


module.exports = app;