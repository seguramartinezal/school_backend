const express = require("express");
const app = express();
const cors = require("cors");
const bodyparser = require("body-parser");
const Connection = require('D:/pruebatecnica/ptecnicabackend/connection/conexion.js');

app.use(cors());
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());

const pool = Connection.pool;

app.get('/Calificacion/Grado', (req, res) => {

    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        const response = await client.query(`SELECT * FROM "Grado"`)

        done();

        if(err) {
            return console.error('error running query', err);
        }
        res.send(response.rows);
    })
})

app.get('/Calificacion/Materia', (req, res) => {

    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        const response = await client.query(`SELECT * FROM "Materia"`)

        done();

        if(err) {
            return console.error('error running query', err);
        }

        res.send(response.rows);
    })

})

app.get('/Calificacion', (req, res) => {


    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        const response = await client.query(`select meg."Id_Materia_Estudiante_Grado" id, e."Nombre" || ' ' || e."Apellido" || ' ' || e."Matricula" estudiante,
        g."Nombre" grado, m."Nombre" materia, c."Nota" from "Materia_Estudiante_Grado" meg join "Estudiante_En_Curso" eec 
        on eec."Id_Estudiante_en_curso" = meg."Id_Estudiante_Curso" join "Estudiante" e on e."Id_estudiante" = eec."Id_Estudiante" 
        join "Grado" g on g."ID_Grado" = eec."Id_Grado"  join "Materia" m on meg."Id_Materia" = m."Id_Materia" 
        left join "Calificacion" c ON c."Id_Estudiante_Grado_Materia" = meg."Id_Materia_Estudiante_Grado"  
        where g."Nombre" = '${req.query.idGrado}' and m."Nombre" = '${req.query.nombreMateria}'`)

        console.log(response.rows);
        done();

        if(err) {
            return console.error('error running query', err);
        }
        res.send(response.rows);
    })

})


app.put("/ActualizarCalificacion", (req, res) => {

    const json = {
        success: false,
        message: ""
    }

    console.log(req.body);

    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        await client.query(`INSERT INTO "Calificacion" ("Id_Estudiante_Grado_Materia", "Id_Materia", "Nota")
                                                        VALUES(${req.body.data.id}, ${req.body.data.materia}, 
                                                        ${req.body.data.nota})`)

        done();

        if(err) {
            json.success = false;
            json.message = "Error al actualizar registro: " + err;
            res.send(json);
            return console.error('error running query', err);
        }

        json.success = true;
        json.message = "Nota actualizada";


        res.send(json);
    })

})







module.exports = app;