const express = require("express");
const app = express();
const cors = require("cors");
const bodyparser = require("body-parser");
const Connection = require('D:/pruebatecnica/ptecnicabackend/connection/conexion.js');



app.use(cors());
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());



const pool = Connection.pool;
const client = Connection.client;


app.get('/ConsultarNota', (req, res) => {
    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        const response = await client.query(`SELECT * FROM public."Nota"`)

        done();

        if(err) {
            return console.error('error running query', err);
        }
        res.send(response.rows);
    })

})

app.post("/RegistrarNota", (req, res) => {



    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        await client.query(`INSERT INTO public."Nota" ("Literal", "Rango_desde", "Rango_hasta")
	                        VALUES ('${req.body.literal}', ${req.body.nota_desde}, ${req.body.nota_hasta})`)

        done();

        if(err) {
            return console.error('error running query', err);
        }

        res.send("true");
    })

})
app.put("/ConsultarNota", (req, res) => {

    const json = {
        success: false,
        message: ""
    }
    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        await client.query(`UPDATE public."Nota" SET "Literal"='${req.body.data.literal}', 
            "Rango_desde"=${req.body.data.rango_desde}, "Rango_hasta"=${req.body.data.rango_hasta}  WHERE "Id_Nota"=${req.body.data.id}`)

        done();

        if(err) {
            json.success = false;
            json.message = "Error al actualizar registro: " + err;
            res.send(json);
            return console.error('error running query', err);
        }

        json.success = true;
        json.message = "Nota actualizada";
        res.send(json);
    })



})

app.delete("/ConsultarNota", (req, res) => {

    const id = req.body.data;
    const json = {
        success: false,
        message: ""
    }

    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        await client.query(`DELETE FROM public."Nota" WHERE "Id_Nota" = ${id}`)

        done();

        if(err) {
            json.success = false;
            json.message = "Error al borrar el registro: " + err;
            res.send(json);
            return console.error('error running query', err);
        }

        json.success = true;
        json.message = "Nota borrada";
        res.send(json);
    })


})


module.exports = app;