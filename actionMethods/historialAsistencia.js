const express = require("express");
const app = express();
const cors = require("cors");
const bodyparser = require("body-parser");
const Connection = require('D:/pruebatecnica/ptecnicabackend/connection/conexion.js');

app.use(cors());
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());

const pool = Connection.pool;

app.get('/HistorialAsistencia/Grado', (req, res) => {

    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        const response = await client.query(`SELECT * FROM "Grado"`)

        done();

        if(err) {
            return console.error('error running query', err);
        }
        res.send(response.rows);
    })
})

app.get('/HistorialAsistencia/Materia', (req, res) => {

    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        const response = await client.query(`SELECT * FROM "Materia"`)

        done();

        if(err) {
            return console.error('error running query', err);
        }

        res.send(response.rows);
    })

})

app.get('/HistorialAsistencia', (req, res) => {

    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        const response = await client.query(`select meg."Id_Materia_Estudiante_Grado" id, e."Nombre" || ' ' || e."Apellido" || ' ' || e."Matricula" estudiante,
        g."Nombre" grado, m."Nombre" materia,
        a."Asistencia" asistencia,
        a."Fecha" fecha 
        from "Materia_Estudiante_Grado" meg 
        join "Estudiante_En_Curso" eec on eec."Id_Estudiante_en_curso" = meg."Id_Estudiante_Curso"
        join "Estudiante" e on e."Id_estudiante" = eec."Id_Estudiante" 
        join "Grado" g on g."ID_Grado" = eec."Id_Grado"  
        join "Materia" m on meg."Id_Materia" = m."Id_Materia"
        join "Asistencia" a on a."Id_Materia_Estudiante_Grado" = meg."Id_Materia_Estudiante_Grado"
        where g."Nombre" = '${req.query.idGrado}' and m."Nombre" = '${req.query.nombreMateria}' and a."Fecha" = '${req.query.fecha}'`)


        done();

        if(err) {
            return console.error('error running query', err);
        }
        res.send(response.rows);
    })

})









module.exports = app;