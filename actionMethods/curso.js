const express = require("express");
const app = express();
const cors = require("cors");
const bodyparser = require("body-parser");
const Connection = require('D:/pruebatecnica/ptecnicabackend/connection/conexion.js');



app.use(cors());
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());



const pool = Connection.pool;
const client = Connection.client;


app.get('/ConsultarGrado', (req, res) => {
    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        const response = await client.query(`SELECT * FROM public."Grado"`)

        done();

        if(err) {
            return console.error('error running query', err);
        }
        res.send(response.rows);
    })

})

app.post("/RegistrarGrado", (req, res) => {
    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        await client.query(`INSERT INTO public."Grado" ("Nombre") VALUES ('${req.body.nombre}')`)

        done();

        if(err) {
            return console.error('error running query', err);
        }

        res.send("true");
    })





})
app.put("/ConsultarGrado", (req, res) => {

    const json = {
        success: false,
        message: ""
    }

    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        await client.query(`UPDATE public."Grado" SET "Nombre"='${req.body.data.nombre}' WHERE "ID_Grado"=${req.body.data.id}`)

        done();

        if(err) {
            json.success = false;
            json.message = "Error al actualizar registro: " + err;
            res.send(json);
            return console.error('error running query', err);
        }

        json.success = true;
        json.message = "Grado actualizado";
        res.send(json);
    })


})

app.delete("/ConsultarGrado", (req, res) => {

    const id = req.body.data;
    const json = {
        success: false,
        message: ""
    }

    return pool.connect(async (err, client, done) => {

        if(err) {
            return console.error('connexion error', err);
        }

        await client.query(`DELETE FROM public."Grado" WHERE "ID_Grado" = ${id}`)

        done();

        if(err) {
            json.success = false;
            json.message = "Error al borrar el registro: " + err;
            res.send(json);
            return console.error('error running query', err);
        }

        json.success = true;
        json.message = "Grado borrado";
        res.send(json);
    })


})


module.exports = app;