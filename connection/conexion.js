const {Pool, Client} = require("pg");

const Connection = {}


const PoolMethod = () => {

    const pool = new Pool({
        user: 'postgres',
        host: 'localhost',
        database: 'school',
        password: '12345',
        port: 5432
    })

    return pool;
}


const ClientMethod = () => {

    const client = new Client({
        user: 'postgres',
        host: 'localhost',
        database: 'school',
        password: '12345',
        port: 5432,
    })

    return client;
}

Connection.pool = PoolMethod();
Connection.client = ClientMethod();

module.exports = Connection;
